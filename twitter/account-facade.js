var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../../potaore/artifact');
var acm = require('../account/account-manager-client');
var Twitter = require('twit');
var uuid = require('node-uuid');
var TwitterAccountFacade = (function (_super) {
    __extends(TwitterAccountFacade, _super);
    function TwitterAccountFacade(host, port, tconfig) {
        _super.call(this, 'TwitterAccountFacade');
        this.acmClient = new acm.AccountManagerClient(host, port);
        this.twitter = new Twitter(tconfig);
    }
    TwitterAccountFacade.prototype.start = function () {
        var _this = this;
        var stream = this.twitter.stream('user');
        stream.on('direct_message', function (data) {
            var id_str = data.direct_message.sender.id_str;
            if (data.direct_message.sender.id != 3776514733) {
                var account = {
                    name: data.direct_message.sender.name,
                    profile_url: data.direct_message.sender.profile_image_url,
                    tid: data.direct_message.sender.id + "",
                    password: uuid.v4().split('-')[0]
                };
                var tweet = function () {
                    _this.notify('upsert-success-tweet', account);
                    var message = "認証文字列は以下です。\n" + account.tid + ':' + account.password;
                    _this.twitter.post('direct_messages/new', { user_id: id_str, text: message }, function (data) { });
                };
                var etweet = function () {
                    _this.notify('upsert-error-tweet', account);
                    var message = 'アカウント登録処理に失敗しました。時間を置いて再度試してください。';
                    _this.twitter.post('direct_messages/new', { user_id: id_str, text: message }, function (data) { });
                };
                _this.acmClient.getAccount(account.tid, 
                // already exists -> update account
                function (res) {
                    _this.acmClient.updateAccount(account, function (result) { return tweet(); }, function (err) { return etweet(); });
                }, 
                // not found -> insert account
                function (err) {
                    account.games = [];
                    account.results = {
                        "free": { "win": 0, "loose": 0, "draw": 0 },
                        "rating": { "win": 0, "loose": 0, "draw": 0 }
                    };
                    account.rate = 1500;
                    account.rank = 0;
                    account.point = 0;
                    _this.acmClient.createAccount(account, function (result) { return tweet(); }, function () { return etweet(); });
                });
            }
        });
    };
    return TwitterAccountFacade;
})(Artifact.Artifact);
exports.TwitterAccountFacade = TwitterAccountFacade;
