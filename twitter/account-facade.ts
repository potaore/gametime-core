declare function require(x: string): any;

import Artifact = require('../../potaore/artifact');
import acm = require('../account/account-manager-client');
let Twitter = require('twit');
let uuid = require('node-uuid');

export class TwitterAccountFacade extends Artifact.Artifact {
  twitter: any;
  acmClient: acm.AccountManagerClient;

  constructor(host: string, port: string, tconfig) {
    super('TwitterAccountFacade');
    this.acmClient = new acm.AccountManagerClient(host, port);
    this.twitter = new Twitter(tconfig);
  }

  start() {
    let stream = this.twitter.stream('user');
    stream.on('direct_message', (data) => {
      let id_str = data.direct_message.sender.id_str;
      if (data.direct_message.sender.id != 3776514733) {
        let account: any = {
          name: data.direct_message.sender.name,
          profile_url: data.direct_message.sender.profile_image_url,
          tid: data.direct_message.sender.id + "",
          password: uuid.v4().split('-')[0]
        };
        let tweet = () => {
          this.notify('upsert-success-tweet', account);
          let message = "認証文字列は以下です。\n" + account.tid + ':' + account.password;
          this.twitter.post('direct_messages/new', { user_id: id_str, text: message }, data => { });
        };

        let etweet = () => {
          this.notify('upsert-error-tweet', account);
          let message = 'アカウント登録処理に失敗しました。時間を置いて再度試してください。';
          this.twitter.post('direct_messages/new', { user_id: id_str, text: message }, data => { });
        }

        this.acmClient.getAccount(account.tid,
          // already exists -> update account
          (res) => {
            this.acmClient.updateAccount(account, result => tweet(), err => etweet() );
          },
          // not found -> insert account
          (err) => {
            account.games = []
            account.results = {
              "free": { "win": 0, "loose": 0, "draw": 0 },
              "rating": { "win": 0, "loose": 0, "draw": 0 }
            };
            account.rate = 1500
            account.rank = 0
            account.point = 0
            this.acmClient.createAccount(
              account,
              result => tweet(),
              () => etweet()
            );
          }
        );
      }
    });
  }
}