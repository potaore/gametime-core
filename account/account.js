var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../../potaore/artifact');
var express = require('express');
var bodyParser = require('body-parser');
var log4js = require('log4js');
var uuid = require('node-uuid');
var Account = (function () {
    function Account(rawData) {
        this.rawData = rawData;
        this.filteredData = JSON.parse(JSON.stringify(rawData), function (key, value) {
            if (key.indexOf('_') === 0)
                return undefined;
            else if (key == 'password')
                return undefined;
            else
                return value;
        });
        this.auth = false;
    }
    Account.prototype.startSession = function () {
        this.auth = true;
        this.cid = uuid.v4();
    };
    Account.prototype.endSession = function () {
        this.auth = false;
        this.cid = '';
    };
    Account.prototype.getData = function () {
        return {
            data: this.filteredData,
            auth: this.auth
        };
    };
    Account.prototype.getDataWithCid = function () {
        return {
            data: this.filteredData,
            auth: this.auth,
            cid: this.cid
        };
    };
    return Account;
})();
exports.Account = Account;
var AccountManager = (function (_super) {
    __extends(AccountManager, _super);
    function AccountManager(accessor, port) {
        var _this = this;
        _super.call(this, 'AccountManager');
        this.accessor = accessor;
        this.accounts = {};
        this.cidCache = {};
        this.port = port;
        this.exapp = express();
        this.exapp.use(bodyParser.json());
        this.exapp.get('/account/find', function (req, res) {
            _this.notify('receive-request-find', { request: req });
            if (!req.query.tid) {
                _this.notify('receive-request-find-error', { message: 'tid is empty' });
                res.json({ err: true });
            }
            _this.getAccountByTid(req.query.tid, function (account) {
                _this.notify('receive-request-find-success', { request: req });
                res.json({ result: account.getData() });
            }, function () {
                _this.notify('receive-request-find-error', { message: 'not found' });
                res.json({ err: true });
            });
        });
        this.exapp.post('/account/create', function (req, res) {
            _this.notify('receive-request-create', { request: req.body });
            _this.accessor.insert(req.body, function (result) {
                _this.notify('receive-request-create-success', { request: result });
                res.json({ result: result });
            }, function (err) {
                _this.notify('receive-request-create-err', { request: err });
                res.json({ err: err });
            });
        });
        this.exapp.post('/account/update', function (req, res) {
            _this.notify('receive-request-update', { request: req.body });
            _this.getAccountByTid(req.body.tid, function (account) {
                account.needRefresh = true;
                req.body._id = account.rawData._id;
                _this.accessor.update(req.body, function (result) {
                    _this.notify('receive-request-update-success', { result: result });
                    res.json({ result: true });
                }, function (err) {
                    _this.notify('receive-request-update-error', { result: err });
                    res.json({ err: true });
                });
            }, function () { return res.json({ err: true }); });
        });
        this.exapp.post('/account/login', function (req, res) {
            _this.notify('receive-request-login', { request: req.body });
            if (req.body.cid) {
                _this.getAccountByCid(req.body.cid, function (account) { return res.json({ result: account.getDataWithCid() }); }, function () { return res.json({ result: false }); });
            }
            else if (req.body.tid) {
                _this.login(req.body.tid, req.body.password, function (account) { return res.json({ result: account.getDataWithCid() }); }, function () { return res.json({ result: false }); });
            }
            else {
                res.json({ result: true });
            }
        });
        this.exapp.post('/account/logout', function (req, res) {
            _this.notify('receive-request-logout', { request: req.body });
            _this.logout(req.body.tid);
            res.json({ result: true });
        });
    }
    AccountManager.prototype.getAccountByTid = function (tid, cont, econt) {
        var _this = this;
        if (tid == "" || tid == undefined || tid == null) {
            econt();
            return;
        }
        if (this.accounts.hasOwnProperty(tid) && !this.accounts[tid].needRefresh) {
            cont(this.accounts[tid]);
            return;
        }
        this.accessor.findSingle({ tid: tid }, function (rawData) {
            _this.accounts[tid] = new Account(rawData);
            _this.cidCache[_this.accounts[tid].cid] = tid;
            cont(_this.accounts[tid]);
        }, function (err) { return econt(); });
    };
    AccountManager.prototype.login = function (tid, password, cont, econt) {
        this.getAccountByTid(tid, function (account) {
            if (account.rawData.password == password) {
                account.startSession();
                cont(account);
            }
            else {
                econt();
            }
        }, function () { return econt(); });
    };
    AccountManager.prototype.logout = function (tid) {
        var _this = this;
        this.getAccountByTid(tid, function (account) {
            _this.cidCache[account.cid] = undefined;
            account.endSession();
        }, function () { });
    };
    AccountManager.prototype.getAccountByCid = function (cid, cont, econt) {
        if (this.cidCache.hasOwnProperty(cid)) {
            var account = this.accounts[this.cidCache[cid]];
            if (account.auth && account.cid == cid) {
                cont(account);
            }
            else {
                this.cidCache[cid] = undefined;
                econt();
            }
        }
        else {
            econt();
        }
    };
    AccountManager.prototype.start = function () {
        this.exapp.listen(this.port);
    };
    return AccountManager;
})(Artifact.Artifact);
exports.AccountManager = AccountManager;
