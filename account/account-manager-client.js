var restClient = require('node-rest-client').Client;
var AccountManagerClient = (function () {
    function AccountManagerClient(host, port) {
        this.client = new restClient();
        this.host = host;
        this.port = port;
        this.rootUri = 'http://' + host + ':' + port + '/account/';
    }
    AccountManagerClient.prototype.parseToJson = function (data, cont, econt) {
        try {
            cont(JSON.parse(data));
        }
        catch (e) {
            econt(e);
        }
    };
    AccountManagerClient.prototype.getAccount = function (tid, cont, econt) {
        var _this = this;
        if (cont === void 0) { cont = function (account) { }; }
        if (econt === void 0) { econt = function (err) { }; }
        this.client.get(this.rootUri + 'find?tid=' + tid, function (data, response) {
            _this.parseToJson(data, function (res) { return res.result ? cont(res.result) : econt(res); }, function (err) { return econt(err); });
        });
    };
    AccountManagerClient.prototype.createAccount = function (account, cont, econt) {
        if (cont === void 0) { cont = function (account) { }; }
        if (econt === void 0) { econt = function (err) { }; }
        this.client.post(this.rootUri + 'create', { data: account, headers: { "Content-Type": "application/json" } }, function (data, response) {
            var datajson = JSON.parse(data);
            if (datajson.result) {
                cont(datajson.result);
            }
            else {
                econt(datajson);
            }
        });
    };
    AccountManagerClient.prototype.updateAccount = function (account, cont, econt) {
        if (cont === void 0) { cont = function (account) { }; }
        if (econt === void 0) { econt = function (err) { }; }
        this.client.post(this.rootUri + 'update', { data: account, headers: { "Content-Type": "application/json" } }, function (data, response) {
            var datajson = JSON.parse(data);
            if (datajson.result) {
                cont(datajson.result);
            }
            else {
                econt(datajson);
            }
        });
    };
    AccountManagerClient.prototype.login = function (tid, password, cont, econt) {
        if (cont === void 0) { cont = function (account) { }; }
        if (econt === void 0) { econt = function (err) { }; }
        this.client.post(this.rootUri + 'login', { data: { tid: tid, password: password }, headers: { "Content-Type": "application/json" } }, function (data, response) {
            var datajson = JSON.parse(data);
            if (datajson.result) {
                cont(datajson.result);
            }
            else {
                econt(datajson);
            }
        });
    };
    AccountManagerClient.prototype.loginByCid = function (cid, cont, econt) {
        if (cont === void 0) { cont = function (account) { }; }
        if (econt === void 0) { econt = function (err) { }; }
        this.client.post(this.rootUri + 'login', { data: { cid: cid }, headers: { "Content-Type": "application/json" } }, function (data, response) {
            var datajson = JSON.parse(data);
            if (datajson.result) {
                cont(datajson.result);
            }
            else {
                econt(datajson);
            }
        });
    };
    AccountManagerClient.prototype.logout = function (tid, cont, econt) {
        if (cont === void 0) { cont = function (account) { }; }
        if (econt === void 0) { econt = function (err) { }; }
        this.client.post(this.rootUri + 'logout', { data: { tid: tid }, headers: { "Content-Type": "application/json" } }, function (data, response) {
            var datajson = JSON.parse(data);
            var insertData = datajson.ops ? datajson.ops[0] : null;
            if (insertData) {
                cont(insertData);
            }
            else {
                econt(datajson);
            }
        });
    };
    return AccountManagerClient;
})();
exports.AccountManagerClient = AccountManagerClient;
