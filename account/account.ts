declare function require(x: string): any;

import mongodb = require('../db/db');
import dbService = require('../dbService/dbService');
import Artifact = require('../../potaore/artifact');

import dbAccessor = require('../dbAccessor/dbAccessor');



let express = require('express');
let bodyParser = require('body-parser');
let log4js = require('log4js');
let uuid = require('node-uuid');

export class Account {
  rawData: any;
  filteredData: any;
  auth: boolean;
  cid: string;
  needRefresh: boolean;
  constructor(rawData: any) {
    this.rawData = rawData;
    this.filteredData = JSON.parse(JSON.stringify(rawData), (key, value) => {
      if (key.indexOf('_') === 0) return undefined;
      else if (key == 'password') return undefined;
      else return value;
    });
    this.auth = false;
  }

  startSession() {
    this.auth = true;
    this.cid = uuid.v4();
  }
  endSession() {
    this.auth = false;
    this.cid = '';
  }
  getData() {
    return {
      data: this.filteredData,
      auth: this.auth
    }
  }

  getDataWithCid() {
    return {
      data: this.filteredData,
      auth: this.auth,
      cid: this.cid
    }
  }
}

export class AccountManager extends Artifact.Artifact {
  accessor: dbAccessor.DbAccessor;
  accounts: { [id: string]: Account; };
  cidCache: { [id: string]: string; };
  port: number;
  exapp: any;
  constructor(accessor: dbAccessor.DbAccessor, port: number) {
    super('AccountManager');
    this.accessor = accessor;
    this.accounts = {};
    this.cidCache = {};
    this.port = port;
    this.exapp = express();
    this.exapp.use(bodyParser.json());

    this.exapp.get('/account/find', (req, res) => {
      this.notify('receive-request-find', { request: req });
      if (!req.query.tid) {
        this.notify('receive-request-find-error', { message: 'tid is empty' });
        res.json({ err: true });
      }
      this.getAccountByTid(req.query.tid,
        (account) => {
          this.notify('receive-request-find-success', { request: req });
          res.json({ result: account.getData() });
        },
        () => {
          this.notify('receive-request-find-error', { message: 'not found' });
          res.json({ err: true });
        });
    });

    this.exapp.post('/account/create', (req, res) => {
      this.notify('receive-request-create', { request: req.body });
      this.accessor.insert(req.body,
        (result) => {
          this.notify('receive-request-create-success', { request: result });
          res.json({ result: result });
        },
        (err) => {
          this.notify('receive-request-create-err', { request: err });
          res.json({ err: err })
        }
      );
    });

    this.exapp.post('/account/update', (req, res) => {
      this.notify('receive-request-update', { request: req.body });
      this.getAccountByTid(req.body.tid,
        (account) => {
          account.needRefresh = true;
          req.body._id = account.rawData._id;
          this.accessor.update(req.body,
            (result) => {
              this.notify('receive-request-update-success', { result: result });
              res.json({ result: true });
            },
            (err) => {
              this.notify('receive-request-update-error', { result: err });
              res.json({ err: true });
            }
          );
        },
        () => res.json({ err: true }));
    });

    this.exapp.post('/account/login', (req, res) => {
      this.notify('receive-request-login', { request: req.body });
      if (req.body.cid) {
        this.getAccountByCid(req.body.cid, (account) => res.json({ result: account.getDataWithCid() }), () => res.json({ result: false }));
      } else if (req.body.tid) {
        this.login(req.body.tid, req.body.password, (account) => res.json({ result: account.getDataWithCid() }), () => res.json({ result: false }));
      } else {
        res.json({ result: true });
      }
    });

    this.exapp.post('/account/logout', (req, res) => {
      this.notify('receive-request-logout', { request: req.body });
      this.logout(req.body.tid);
      res.json({ result: true });
    });
  }

  getAccountByTid(tid: string, cont: { (account: Account): any }, econt: { (): any }) {
    if (tid == "" || tid == undefined || tid == null) {
      econt();
      return;
    }
    if (this.accounts.hasOwnProperty(tid) && !this.accounts[tid].needRefresh) {
      cont(this.accounts[tid]);
      return;
    }
    this.accessor.findSingle({ tid: tid },
      (rawData) => {
        this.accounts[tid] = new Account(rawData);
        this.cidCache[this.accounts[tid].cid] = tid;
        cont(this.accounts[tid]);
      },
      (err) => econt()
    );
  }

  login(tid: string, password: string, cont: { (account: Account): any }, econt: { (): any }) {
    this.getAccountByTid(tid, (account) => {
      if (account.rawData.password == password) {
        account.startSession();
        cont(account);
      } else {
        econt();
      }
    }, () => econt());
  }

  logout(tid: string) {
    this.getAccountByTid(tid,
      (account) => {
        this.cidCache[account.cid] = undefined;
        account.endSession();
      },
      () => { }
    );
  }

  getAccountByCid(cid: string, cont: { (account: Account): any }, econt: { (): any }) {
    if (this.cidCache.hasOwnProperty(cid)) {
      let account = this.accounts[this.cidCache[cid]];
      if (account.auth && account.cid == cid) {
        cont(account);
      } else {
        this.cidCache[cid] = undefined;
        econt();
      }
    } else {
      econt();
    }
  }

  start() {
    this.exapp.listen(this.port);
  }
}
