declare function require(x: string): any;
let restClient = require('node-rest-client').Client;

export class AccountManagerClient {
  client: any;
  host: string;
  port: string;
  rootUri: string;
  constructor(host, port) {
    this.client = new restClient();
    this.host = host;
    this.port = port;
    this.rootUri = 'http://' + host + ':' + port + '/account/';
  }
  
  parseToJson(data, cont, econt) {
    try {
      cont(JSON.parse(data))
    } catch (e) {
      econt(e)
    }
  }
  
  getAccount(tid: string, cont = (account) => { }, econt = (err) => { }) {
    this.client.get(this.rootUri + 'find?tid=' + tid, (data, response) => {
      this.parseToJson(data,
        (res) => res.result ? cont(res.result) : econt(res),
        (err) => econt(err)
      );
    });
  }
  
  createAccount(account, cont = (account) => { }, econt = (err) => { }) {
    this.client.post(this.rootUri + 'create',
      { data: account, headers: { "Content-Type": "application/json" } },
      (data, response) => {
        let datajson = data
        if (datajson.result) {
          cont(datajson.result);
        } else {
          econt(datajson);
        }
      }
    );
  }

  updateAccount(account, cont = (account) => { }, econt = (err) => { }) {
    this.client.post(this.rootUri + 'update',
      { data: account, headers: { "Content-Type": "application/json" } },
      (data, response) => {
        let datajson = data
        if (datajson.result) {
          cont(datajson.result);
        } else {
          econt(datajson);
        }
      }
    );
  }
  
  login(tid : string, password : string, cont = (account) => { }, econt = (err) => { }) {
    this.client.post(this.rootUri + 'login',
      { data: {tid :tid, password : password}, headers: { "Content-Type": "application/json" } },
      (data, response) => {
        let datajson = data
        if (datajson.result) {
          cont(datajson.result);
        } else {
          econt(datajson);
        }
      }
    );
  }
  
  loginByCid(cid : string, cont = (account) => { }, econt = (err) => { }) {
    this.client.post(this.rootUri + 'login',
      { data: {cid :cid}, headers: { "Content-Type": "application/json" } },
      (data, response) => {
        let datajson = data
        if (datajson.result) {
          cont(datajson.result);
        } else {
          econt(datajson);
        }
      }
    );
  }
  
  logout(tid : string, cont = (account) => { }, econt = (err) => { }) {
    this.client.post(this.rootUri + 'logout',
      { data: {tid :tid}, headers: { "Content-Type": "application/json" } },
      (data, response) => {
        let datajson = data
        let insertData = datajson.ops ? datajson.ops[0] : null;
        if (insertData) {
          cont(insertData);
        } else {
          econt(datajson);
        }
      }
    );
  }
}