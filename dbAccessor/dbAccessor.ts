declare function require(x: string): any;

let Client = require('node-rest-client').Client;


let baseUrl = "http://localhost:10626/gametime/";

export class DbAccessor {
  url: string;
  dbName: string;
  collectionName: string;
  path: string;
  restClient = new Client();
  constructor(url: string, dbName: string, collectionName: string) {
    this.url = url;
    this.dbName = dbName;
    this.collectionName = collectionName;
    this.path = this.url + this.dbName + '/' + this.collectionName + '/';
  }

  find(request, cont, econt = (err) =>{}) {
    let paramstr = '?';
    let notfirst = false;
    for (let key in request) {
      if (notfirst) paramstr += '&';
      notfirst = true;
      paramstr += key + '=' + request[key];
    }
    this.restClient.get(this.path + 'find' + paramstr, (data, response) => {
      this.parseToJson(data,
        (res) => res.result ? cont(res.result) : econt(res),
        (err) => econt(err)
      );
    }).on('error', (err) => econt(err));
  }

  findSingle(request, cont, econt = (err) =>{}) {
    request['__single'] = true;
    this.find(request, cont, econt);
  }

  findDirect(request, cont, econt = (err) =>{}) {
    this.restClient.post(this.path + 'findDirect',
      { data: request, headers: { "Content-Type": "application/json" } },
      (data, response) => {
        let datajson = JSON.parse(data);
        if (datajson.result) {
          cont(datajson.result);
        } else {
          econt(datajson);
        }
      }
    ).on('error', (err) => econt(err));
  }

  insert(request, cont, econt = (err) =>{}) {
    this.restClient.post(this.path + 'insert',
      { data: request, headers: { "Content-Type": "application/json" } },
      (data, response) => {
        let datajson = JSON.parse(data)
        if (datajson.result) {
          let insertData = datajson.result.ops ? datajson.result.ops[0] : null;
          if (insertData)
            cont(insertData)
          else
            econt(datajson);
        } else {
          econt(datajson);
        }
      }
    ).on('error', (err) => econt(err));
  }

  update(request, cont, econt = (err) =>{}) {
    this.restClient.post(this.path + 'update',
      { data: request, headers: { "Content-Type": "application/json" } },
      (data, response) => {
        let datajson = JSON.parse(data);
        if (datajson) {
          cont(datajson)
        } else {
          econt(response);
        }
      }
    ).on('error', (err) => econt(err));
  }


  parseToJson(data, cont, econt = (err) =>{}) {
    try {
      cont(JSON.parse(data))
    } catch (e) {
      econt(e)
    }
  }
}

