var Client = require('node-rest-client').Client;
var baseUrl = "http://localhost:10626/gametime/";
var DbAccessor = (function () {
    function DbAccessor(url, dbName, collectionName) {
        this.restClient = new Client();
        this.url = url;
        this.dbName = dbName;
        this.collectionName = collectionName;
        this.path = this.url + this.dbName + '/' + this.collectionName + '/';
    }
    DbAccessor.prototype.find = function (request, cont, econt) {
        var _this = this;
        if (econt === void 0) { econt = function (err) { }; }
        var paramstr = '?';
        var notfirst = false;
        for (var key in request) {
            if (notfirst)
                paramstr += '&';
            notfirst = true;
            paramstr += key + '=' + request[key];
        }
        this.restClient.get(this.path + 'find' + paramstr, function (data, response) {
            _this.parseToJson(data, function (res) { return res.result ? cont(res.result) : econt(res); }, function (err) { return econt(err); });
        }).on('error', function (err) { return econt(err); });
    };
    DbAccessor.prototype.findSingle = function (request, cont, econt) {
        if (econt === void 0) { econt = function (err) { }; }
        request['__single'] = true;
        this.find(request, cont, econt);
    };
    DbAccessor.prototype.findDirect = function (request, cont, econt) {
        if (econt === void 0) { econt = function (err) { }; }
        this.restClient.post(this.path + 'findDirect', { data: request, headers: { "Content-Type": "application/json" } }, function (data, response) {
            var datajson = JSON.parse(data);
            if (datajson.result) {
                cont(datajson.result);
            }
            else {
                econt(datajson);
            }
        }).on('error', function (err) { return econt(err); });
    };
    DbAccessor.prototype.insert = function (request, cont, econt) {
        if (econt === void 0) { econt = function (err) { }; }
        this.restClient.post(this.path + 'insert', { data: request, headers: { "Content-Type": "application/json" } }, function (data, response) {
            var datajson = JSON.parse(data);
            if (datajson.result) {
                var insertData = datajson.result.ops ? datajson.result.ops[0] : null;
                if (insertData)
                    cont(insertData);
                else
                    econt(datajson);
            }
            else {
                econt(datajson);
            }
        }).on('error', function (err) { return econt(err); });
    };
    DbAccessor.prototype.update = function (request, cont, econt) {
        if (econt === void 0) { econt = function (err) { }; }
        this.restClient.post(this.path + 'update', { data: request, headers: { "Content-Type": "application/json" } }, function (data, response) {
            var datajson = JSON.parse(data);
            if (datajson) {
                cont(datajson);
            }
            else {
                econt(response);
            }
        }).on('error', function (err) { return econt(err); });
    };
    DbAccessor.prototype.parseToJson = function (data, cont, econt) {
        if (econt === void 0) { econt = function (err) { }; }
        try {
            cont(JSON.parse(data));
        }
        catch (e) {
            econt(e);
        }
    };
    return DbAccessor;
})();
exports.DbAccessor = DbAccessor;
