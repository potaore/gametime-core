declare function require(x: string): any;

import mongodb = require('../db/db');
import dbService = require('../dbService/dbService');
import Artifact = require('../../potaore/artifact');

//setup logger
let log4js = require('log4js');
log4js.configure('start-db-server.log4js.json');
let logger = log4js.getLogger("normal-logger");

//for test
let restClient = require('node-rest-client').Client
let client = new restClient()


//setup and start db service
let db = new mongodb.DB('gametime');
let accountService = new dbService.DbService(db, 'gametime', 'account', 10627);
let tsuitateGameService = new dbService.DbService(db, 'gametime', 'game', 10628);
let listener = new Artifact.Listner(logger, true);
listener.listen(db);
listener.listen(tsuitateGameService);
accountService.start();
tsuitateGameService.start();

