var mongodb = require('../db/db');
var dbService = require('../dbService/dbService');
var Artifact = require('../../potaore/artifact');
//setup logger
var log4js = require('log4js');
log4js.configure('start-db-server.log4js.json');
var logger = log4js.getLogger("normal-logger");
//for test
var restClient = require('node-rest-client').Client;
var client = new restClient();
//setup and start db service
var db = new mongodb.DB('gametime');
var accountService = new dbService.DbService(db, 'gametime', 'account', 10627);
var tsuitateGameService = new dbService.DbService(db, 'gametime', 'game', 10628);
var listener = new Artifact.Listner(logger, true);
listener.listen(db);
listener.listen(tsuitateGameService);
accountService.start();
tsuitateGameService.start();
