var taf = require('../twitter/account-facade');
var Artifact = require('../../potaore/artifact');
//setup logger
var log4js = require('log4js');
log4js.configure('start-twitter-account-facade.log4js.json');
var logger = log4js.getLogger("normal-logger");
var cfg = require('./start-twitter-account-facade.config.json');
var twitterAccountFacade = new taf.TwitterAccountFacade('localhost', '12121', cfg);
var listener = new Artifact.Listner(logger, true);
listener.listen(twitterAccountFacade);
twitterAccountFacade.start();
