var dbAccessor = require('../dbAccessor/dbAccessor');
var account = require('../account/account');
var Artifact = require('../../potaore/artifact');
//setup logger
var log4js = require('log4js');
log4js.configure('start-account-manager.log4js.json');
var logger = log4js.getLogger("normal-logger");
//
var acccessor = new dbAccessor.DbAccessor('http://localhost:10627/', 'gametime', 'account');
var manager = new account.AccountManager(acccessor, 12121);
var listener = new Artifact.Listner(logger, true);
listener.listen(manager);
manager.start();
