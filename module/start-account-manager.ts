declare function require(x: string): any;

import dbAccessor = require('../dbAccessor/dbAccessor');
import account = require('../account/account');
import Artifact = require('../../potaore/artifact');

//setup logger
let log4js = require('log4js');
log4js.configure('start-account-manager.log4js.json');
let logger = log4js.getLogger("normal-logger");

//
let acccessor = new dbAccessor.DbAccessor('http://localhost:10627/', 'gametime', 'account');
let manager = new account.AccountManager(acccessor, 12121);
let listener = new Artifact.Listner(logger, true);
listener.listen(manager);
manager.start();


