declare function require(x: string): any;




import taf = require('../twitter/account-facade');
import Artifact = require('../../potaore/artifact');

//setup logger
let log4js = require('log4js');
log4js.configure('start-twitter-account-facade.log4js.json');
let logger = log4js.getLogger("normal-logger");

let cfg = require('./start-twitter-account-facade.config.json');
let twitterAccountFacade = new taf.TwitterAccountFacade('localhost', '12121', cfg);
let listener = new Artifact.Listner(logger, true);
listener.listen(twitterAccountFacade);
twitterAccountFacade.start();