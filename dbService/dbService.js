var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../../potaore/artifact');
var express = require('express');
var bodyParser = require('body-parser');
var DbService = (function (_super) {
    __extends(DbService, _super);
    function DbService(db, dbName, collectionName, port) {
        _super.call(this, 'DB Service of ' + '/' + dbName + '/' + collectionName);
        this.started = false;
        this.db = db;
        this.dbName = dbName;
        this.collectionName = collectionName;
        this.port = port;
    }
    DbService.prototype.start = function () {
        var _this = this;
        var app = express();
        app.use(bodyParser.json());
        if (this.started)
            return;
        this.started = true;
        this.db.connectToDb();
        var clpath = '/' + this.dbName + '/' + this.collectionName;
        this.notify('start-listen-find', { path: clpath + '/find' });
        app.get(clpath + '/find', function (req, res) {
            _this.notify('receive-request-find', { request: req.query });
            _this.db.find(_this.collectionName, req.query, function (ret) {
                //this.notify('receive-request-find-data', { data: ret });
                res.json(ret);
            });
        });
        this.notify('start-listen-findDirect', { path: clpath + '/findDirect' });
        app.post(clpath + '/findDirect', function (req, res) {
            _this.notify('receive-request-findDirect', { request: req.body });
            _this.db.findDirect(_this.collectionName, req.body, function (ret) {
                //this.notify('receive-request-findDirect-data', { data: ret });
                res.json(ret);
            });
        });
        this.notify('start-listen-insert', { path: clpath + '/insert' });
        app.post(clpath + '/insert', function (req, res) {
            _this.notify('receive-request-insert', { request: req.body });
            _this.db.insert(_this.collectionName, req.body, function (ret) {
                //this.notify('receive-request-insert-data', { data: ret });
                res.json(ret);
            });
        });
        this.notify('start-listen-update', { path: clpath + '/update' });
        app.post(clpath + '/update', function (req, res) {
            _this.notify('receive-request-update', { request: req.body });
            _this.db.update(_this.collectionName, req.body, function (ret) {
                //this.notify('receive-request-update-data', { data: ret });
                res.json(ret);
            });
        });
        this.notify('start-listen', { port: this.port });
        app.listen(this.port);
    };
    return DbService;
})(Artifact.Artifact);
exports.DbService = DbService;
