declare function require(x: string): any;

import Artifact = require('../../potaore/artifact');
import mongodb = require('../db/db');
let express = require('express');
var bodyParser = require('body-parser');

export class DbService extends Artifact.Artifact {
  db: mongodb.DB;
  dbName: string;
  collectionName: string;
  port: number;
  started: boolean = false;
  constructor(db: mongodb.DB, dbName: string, collectionName: string, port: number) {
    super('DB Service of ' + '/' + dbName + '/' + collectionName);
    this.db = db;
    this.dbName = dbName;
    this.collectionName = collectionName;
    this.port = port;
  }
  start() {
    let app = express();
    app.use(bodyParser.json());
    if (this.started) return;
    this.started = true;
    this.db.connectToDb();

    let clpath = '/' + this.dbName + '/' + this.collectionName;

    this.notify('start-listen-find', { path: clpath + '/find' });
    app.get(clpath + '/find', (req, res) => {
      this.notify('receive-request-find', { request: req.query });
      this.db.find(this.collectionName, req.query, (ret) => {
        //this.notify('receive-request-find-data', { data: ret });
        res.json(ret);
      });
    });

    this.notify('start-listen-findDirect', { path: clpath + '/findDirect' });
    app.post(clpath + '/findDirect', (req, res) => {
      this.notify('receive-request-findDirect', { request: req.body });
      this.db.findDirect(this.collectionName, req.body, (ret) => {
        //this.notify('receive-request-findDirect-data', { data: ret });
        res.json(ret);
      });
    });

    this.notify('start-listen-insert', { path: clpath + '/insert' });
    app.post(clpath + '/insert', (req, res) => {
      this.notify('receive-request-insert', { request: req.body });
      this.db.insert(this.collectionName, req.body, (ret) => {
        //this.notify('receive-request-insert-data', { data: ret });
        res.json(ret);
      });
    });

    this.notify('start-listen-update', { path: clpath + '/update' });
    app.post(clpath + '/update', (req, res) => {
      this.notify('receive-request-update', { request: req.body });
      this.db.update(this.collectionName, req.body, (ret) => {
        //this.notify('receive-request-update-data', { data: ret });
        res.json(ret);
      });
    });

    this.notify('start-listen', { port: this.port });
    app.listen(this.port);
  }
}


