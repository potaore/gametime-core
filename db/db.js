var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var UTIL = require('../../potaore/utility');
var Artifact = require('../../potaore/artifact');
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var ObjectId = mongodb.ObjectId;
var DB = (function (_super) {
    __extends(DB, _super);
    function DB(dbName) {
        _super.call(this, 'mongodb wrapper');
        this.connected = false;
        this.dbName = dbName;
    }
    DB.prototype.putRequestId = function (request) {
        request['request-id'] = UTIL.Utility.uuid();
    };
    DB.prototype.parseToSelectRequest = function (request) {
        var ret = {};
        for (var key in request) {
            if (key == '_id') {
                if (!ret.query)
                    ret.query = {};
                ret.query._id = new ObjectId(request[key]);
            }
            else if (key == '__single')
                ret.single = request[key];
            else if (key == '__skip')
                ret.skip = parseInt(request[key]);
            else if (key == '__limit')
                ret.limit = parseInt(request[key]);
            else if (key.indexOf('__orderby') == 0) {
                if (!ret.orderby)
                    ret.orderby = {};
                ret.orderby[key.replace('__orderby_', '')] = parseInt(request[key]);
            }
            else {
                if (!ret.query)
                    ret.query = {};
                ret.query[key] = request[key];
            }
        }
        this.putRequestId(ret);
        return ret;
    };
    DB.prototype.parseToSelectDirectRequest = function (request) {
        var ret = {};
        for (var key in request) {
            if (key == '$orderby') {
                ret.orderby = request[key];
                for (var orderbyKey in ret.orderby) {
                    ret.orderby[orderbyKey] = parseInt(ret.orderby[orderbyKey]);
                }
            }
            else if (key == '$limit') {
                ret.limit = parseInt(request[key]);
            }
            else if (key == '$skip') {
                ret.skip = parseInt(request[key]);
            }
            else {
                if (!ret.query)
                    ret.query = {};
                ret.query[key] = request[key];
            }
        }
        this.putRequestId(ret);
        return ret;
    };
    DB.prototype.parseToUpsertRequest = function (request) {
        var ret = {};
        ret.filter = { _id: new ObjectId(request._id) };
        ret.query = request;
        delete ret.query._id;
        this.putRequestId(ret);
        return ret;
    };
    DB.prototype.connectToDb = function () {
        var _this = this;
        if (this.connected)
            return;
        MongoClient.connect('mongodb://potaore2:oreoreoreo2@133.130.72.92:27017/' + this.dbName, function (err, db) {
            if (err) {
                _this.notify('failed-to-connect-db', err);
                return;
            }
            _this.connected = true;
            _this.db = db;
        });
    };
    DB.prototype.find = function (collectionName, request, callBack) {
        var _this = this;
        var selectRequest = this.parseToSelectRequest(request);
        this.notify('find-' + collectionName, selectRequest);
        if (selectRequest.single) {
            this.db.collection(collectionName).findOne(selectRequest.query, function (err, result) { return _this.logAndCallBack(selectRequest, err, result, callBack); });
        }
        else {
            var seq;
            if (selectRequest.query) {
                seq = this.db.collection(collectionName)
                    .find({ $query: selectRequest.query });
            }
            else {
                seq = this.db.collection(collectionName).find();
            }
            if (selectRequest.orderby)
                seq.sort(selectRequest.orderby);
            if (selectRequest.skip)
                seq.skip(selectRequest.skip);
            if (selectRequest.limit)
                seq.limit(selectRequest.limit);
            seq.toArray(function (err, result) { return _this.logAndCallBack(selectRequest, err, result, callBack); });
        }
    };
    DB.prototype.findDirect = function (collectionName, request, callBack) {
        var _this = this;
        var selectRequest = this.parseToSelectDirectRequest(request);
        var seq = this.db.collection(collectionName).find(selectRequest.query);
        if (selectRequest.orderby)
            seq.sort(selectRequest.orderby);
        if (selectRequest.skip)
            seq.skip(selectRequest.skip);
        if (selectRequest.limit)
            seq.limit(selectRequest.limit);
        seq.toArray(function (err, result) { return _this.logAndCallBack(selectRequest, err, result, callBack); });
    };
    DB.prototype.insert = function (collectionName, request, callBack) {
        var _this = this;
        this.putRequestId(request);
        this.notify('insert-' + collectionName, request);
        var collection = this.db.collection(collectionName);
        collection.insert(request, function (err, result) {
            _this.logAndCallBack(request, err, result, callBack);
        });
    };
    DB.prototype.update = function (collectionName, request, callBack) {
        var _this = this;
        var updateRequest = this.parseToUpsertRequest(request);
        this.notify('update-' + collectionName, updateRequest);
        var collection = this.db.collection(collectionName);
        collection.updateOne(updateRequest.filter, { $set: updateRequest.query }, function (err, result) {
            _this.logAndCallBack(updateRequest, err, result, callBack);
        });
    };
    DB.prototype.logAndCallBack = function (request, err, result, callBack) {
        var ret = {};
        ret.result = result;
        ret.err = err;
        if (err) {
            this.notify('error-result', { 'request-id': request['request-id'], err: err });
        }
        else {
            this.notify('success-result', { 'request-id': request['request-id'] });
        }
        callBack(ret);
    };
    return DB;
})(Artifact.Artifact);
exports.DB = DB;
