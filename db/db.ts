declare function require(x: string): any;


import UTIL = require('../../potaore/utility');
import Artifact = require('../../potaore/artifact');

let mongodb = require('mongodb');

let MongoClient = mongodb.MongoClient;
let ObjectId = mongodb.ObjectId;


export class DB extends Artifact.Artifact {
  db: any;
  dbName: string;
  connected: boolean = false;
  constructor(dbName: string) {
    super('mongodb wrapper');
    this.dbName = dbName;
  }

  putRequestId(request) {
    request['request-id'] = UTIL.Utility.uuid();
  }

  parseToSelectRequest(request) {
    let ret: any = {};
    for (let key in request) {
      if (key == '_id') {
        if (!ret.query) ret.query = {};
        ret.query._id = new ObjectId(request[key]);
      } else if (key == '__single')
        ret.single = request[key];
      else if (key == '__skip')
        ret.skip = parseInt(request[key]);
      else if (key == '__limit')
        ret.limit = parseInt(request[key]);
      else if (key.indexOf('__orderby') == 0) {
        if (!ret.orderby) ret.orderby = {};
        ret.orderby[key.replace('__orderby_', '')] = parseInt(request[key]);
      } else {
        if (!ret.query) ret.query = {};
        ret.query[key] = request[key];
      }
    }
    this.putRequestId(ret);
    return ret;
  }

  parseToSelectDirectRequest(request) {
    let ret: any = {};
    for(let key in request) {
      if (key == '$orderby') {
        ret.orderby = request[key];
        for(let orderbyKey in ret.orderby) {
          ret.orderby[orderbyKey] = parseInt(ret.orderby[orderbyKey]);
        }
      } else if (key == '$limit') {
        ret.limit = parseInt(request[key]);
      } else if (key == '$skip') {
        ret.skip = parseInt(request[key]);     
      } else {
        if(!ret.query) ret.query = {};
        ret.query[key] = request[key];
      }
    }
    this.putRequestId(ret);
    return ret;
  }

  parseToUpsertRequest(request) {
    let ret: any = {};
    ret.filter = { _id: new ObjectId(request._id) };
    ret.query = request;
    delete ret.query._id;
    this.putRequestId(ret);
    return ret;
  }

  connectToDb() {
    if (this.connected) return;
    MongoClient.connect('mongodb://potaore2:oreoreoreo2@133.130.72.92:27017/' + this.dbName, (err, db) => {
      if (err) {
        this.notify('failed-to-connect-db', err);
        return;
      }
      this.connected = true;
      this.db = db;
    });
  }

  find(collectionName, request, callBack) {
    let selectRequest = this.parseToSelectRequest(request);
    this.notify('find-' + collectionName, selectRequest);
    if (selectRequest.single) {
      this.db.collection(collectionName).findOne(
        selectRequest.query,
        (err, result) => this.logAndCallBack(selectRequest, err, result, callBack)
      );
    } else {
      let seq;
      if (selectRequest.query) {
        seq = this.db.collection(collectionName)
          .find({ $query: selectRequest.query });
      } else {
        seq = this.db.collection(collectionName).find();
      }

      if (selectRequest.orderby) seq.sort(selectRequest.orderby);
      if (selectRequest.skip) seq.skip(selectRequest.skip);
      if (selectRequest.limit) seq.limit(selectRequest.limit);
      seq.toArray((err, result) => this.logAndCallBack(selectRequest, err, result, callBack));
    }
  }

  findDirect(collectionName, request, callBack) {
    let selectRequest = this.parseToSelectDirectRequest(request);
    let seq = this.db.collection(collectionName).find(selectRequest.query);
    if (selectRequest.orderby) seq.sort(selectRequest.orderby);
    if (selectRequest.skip) seq.skip(selectRequest.skip);
    if (selectRequest.limit) seq.limit(selectRequest.limit);
    seq.toArray((err, result) => this.logAndCallBack(selectRequest, err, result, callBack));
  }

  insert(collectionName, request, callBack) {
    this.putRequestId(request);
    this.notify('insert-' + collectionName, request);
    let collection = this.db.collection(collectionName)
    collection.insert(request, (err, result) => {
      this.logAndCallBack(request, err, result, callBack);
    });
  }

  update(collectionName, request, callBack) {
    let updateRequest = this.parseToUpsertRequest(request);
    this.notify('update-' + collectionName, updateRequest);
    let collection = this.db.collection(collectionName);
    collection.updateOne(updateRequest.filter, { $set: updateRequest.query }, (err, result) => {
      this.logAndCallBack(updateRequest, err, result, callBack);
    });
  }

  private logAndCallBack(request, err, result, callBack) {
    let ret: any = {};
    ret.result = result;
    ret.err = err;
    if (err) {
      this.notify('error-result', { 'request-id': request['request-id'], err: err });
    } else {
      this.notify('success-result', { 'request-id': request['request-id'] });
    }
    callBack(ret);
  }
}